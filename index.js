import React from 'react';
import {
  AppRegistry,
} from 'react-native';

import App from './app/App';

export default function flatlistTest() {
  return (
    <App />
  );
}

AppRegistry.registerComponent('flatlistTest', () => flatlistTest);
