import {
  Dimensions,
  Navigator,
  Platform,
 } from 'react-native';

// TODO:: TILE_STYLE_VARS should probably not be a constant as
// we might rotate the screen

const THIS_HOST = 192.168.1.121;
const THIS_PORT = 8013;
export const TILE_STYLE_VARS = {
  tileOuterHeight: Dimensions.get('window').width / 1.3333,
  tileOuterWidth: Dimensions.get('window').width-10,
  tileHeight: (Dimensions.get('window').width / 1.3333),
  tileWidth: (Dimensions.get('window').width)-10,
};

export const TOTAL_NAV_HEIGHT = 0;

export const PROPERTIES_PAGE_SIZE = 24;

export const NAVBAR_BACKGROUND_COLOR = '#13E0b9';
export const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export const API_CONFIG = {
  dev_mode: true,
  domain: `http://${THIS_HOST}:${THIS_PORT}`,
  // propertyPhotosBase: 'https://f000.backblaze.com/file/properties/',
  propertyPhotosBase: `http://${THIS_HOST}:8000/`,
  propertiesBase: '/api/properties',
  locationBase: '/api/location',
};

export const LOAD_STATES = {
  invalid: { name: 'Invalid', index: 0 },
  ioading: { name: 'Loading', index: 1 },
  reloading: { name: 'Reloading', index: 2 },
  valid: { name: 'Valid', index: 3 },
  error: { name: 'Error', index: 4 },
};

export const DATA_ACTIONS = {
  load: { name: 'Load', index: 0 },
  fail: { name: 'Fail', index: 1 },
  succeed: { name: 'Succeed', index: 2 },
};

export const CONTACT_STATE = {
  IDLE : 0,
  STARTED : 1,
  COMPLETE : 2,
  FAILED : 3,
}

export const SEARCH_TYPES = {
  nearby: { title: 'Results Nearby' },
  recent: { title: 'Recently Listed Properties' },
  search: { title: 'Search Results' },
  map: { title: 'Results within Map Area' },
  favorites: { title: 'Favorites' },
};

export const PAGE_TYPE_MAIN = 'mainPage';
export const PAGE_TYPE_SUB = 'subPage';
export const PAGE_TYPE_TOOLBAR = 'toolbar';

export const PAGE_TYPE_MAP = {
  listings: PAGE_TYPE_MAIN,
  searchPage: PAGE_TYPE_MAIN,
  detailView: PAGE_TYPE_SUB,
  register: PAGE_TYPE_SUB,
  signIn: PAGE_TYPE_SUB,
  signInWebview: PAGE_TYPE_SUB,
  profile: PAGE_TYPE_SUB,
  mapDetailView: PAGE_TYPE_SUB,
  menu: PAGE_TYPE_TOOLBAR,
  map: PAGE_TYPE_TOOLBAR,
  search: PAGE_TYPE_TOOLBAR,
};

export const TOOLBAR_ROUTES = {
  menu: { id: 'menu', title: 'Menu' },
  map: { id: 'map', title: 'Map' },
  search: { id: 'search', title: 'Search' },
};

export const CALENDAR_FORMAT = {
  sameDay: 'LT',
  nextDay: '[Tomorrow] LT',
  lastDay: '[Yesterday] LT',
  sameElse: 'MM/DD LT'
};

export const US_STATES = [
  { name: 'Alabama', abbrev: 'AL' },
  { name: 'Alaska', abbrev: 'AK' },
  { name: 'Arizona', abbrev: 'AZ' },
  { name: 'Arkansas', abbrev: 'AR' },
  { name: 'California', abbrev: 'CA' },
  { name: 'Colorado', abbrev: 'CO' },
  { name: 'Connecticut', abbrev: 'CT' },
  { name: 'Delaware', abbrev: 'DE' },
  { name: 'Florida', abbrev: 'FL' },
  { name: 'Georgia', abbrev: 'GA' },
  { name: 'Hawaii', abbrev: 'HI' },
  { name: 'Idaho', abbrev: 'ID' },
  { name: 'Illinois', abbrev: 'IL' },
  { name: 'Indiana', abbrev: 'IN' },
  { name: 'Iowa', abbrev: 'IA' },
  { name: 'Kansas', abbrev: 'KS' },
  { name: 'Kentucky', abbrev: 'KY' },
  { name: 'Louisiana', abbrev: 'LA' },
  { name: 'Maine', abbrev: 'ME' },
  { name: 'Maryland', abbrev: 'MD' },
  { name: 'Massachusetts', abbrev: 'MA' },
  { name: 'Michigan', abbrev: 'MI' },
  { name: 'Minnesota', abbrev: 'MN' },
  { name: 'Mississippi', abbrev: 'MS' },
  { name: 'Missouri', abbrev: 'MO' },
  { name: 'Montana', abbrev: 'MT' },
  { name: 'Nebraska', abbrev: 'NE' },
  { name: 'Nevada', abbrev: 'NV' },
  { name: 'New Hampshire', abbrev: 'NH' },
  { name: 'New Jersey', abbrev: 'NJ' },
  { name: 'New Mexico', abbrev: 'NM' },
  { name: 'New York', abbrev: 'NY' },
  { name: 'North Carolina', abbrev: 'NC' },
  { name: 'North Dakota', abbrev: 'ND' },
  { name: 'Ohio', abbrev: 'OH' },
  { name: 'Oklahoma', abbrev: 'OK' },
  { name: 'Oregon', abbrev: 'OR' },
  { name: 'Pennsylvania', abbrev: 'PA' },
  { name: 'Rhode Island', abbrev: 'RI' },
  { name: 'South Carolina', abbrev: 'SC' },
  { name: 'South Dakota', abbrev: 'SD' },
  { name: 'Tennessee', abbrev: 'TN' },
  { name: 'Texas', abbrev: 'TX' },
  { name: 'Utah', abbrev: 'UT' },
  { name: 'Vermont', abbrev: 'VT' },
  { name: 'Virginia', abbrev: 'VA' },
  { name: 'Washington', abbrev: 'WA' },
  { name: 'West Virginia', abbrev: 'WV' },
  { name: 'Wisconsin', abbrev: 'WI' },
  { name: 'Wyoming', abbrev: 'WY' }
];

export const DUMMY_DATA = [
  {
    title: 'Loading.. 0',
    _id: 'd0',
    address: {
      street_number: '9',
      street_name: 'Field St',
      town: 'Cambridge',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '3'
    },
    bathrooms: {
      number_full: '2',
      number_half: '1'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '2'
    },
    area: {
      square_feet: '1870',
      acres: 0
    },
    property_type: 'Condominium',
    number_units: 10,
    list_price: '1000000',
    amenities: ''
  },
  {
    title: 'Loading.. 1',
    _id: 'd1',
    address: {
      street_number: '55',
      street_name: 'Main St',
      town: 'Arlington',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '4'
    },
    bathrooms: {
      number_full: '3',
      number_half: '1'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '0'
    },
    area: {
      square_feet: '550'
    },
    amenities: ''
  },
  {
    title: 'Loading.. 2',
    _id: 'd2',
    address: {
      street_number: '45',
      street_name: 'Longwood Ave',
      town: 'Brookline',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '3'
    },
    bathrooms: {
      number_full: '2',
      number_half: '1'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '2'
    },
    area: {
      square_feet: '1650'
    },
    amenities: ''
  },
  {
    title: 'Loading.. 3',
    _id: 'd3',
    address: {
      street_number: '350',
      street_name: 'Harvard Ave',
      town: 'Brookline',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '2'
    },
    bathrooms: {
      number_full: '2',
      number_half: '0'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '2'
    },
    area: {
      square_feet: '2300'
    },
    amenities: ''
  },
  {
    title: 'Loading.. 4',
    _id: 'd4',
    address: {
      street_number: '275',
      street_name: 'Tappan St',
      town: 'Brookline',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '3'
    },
    bathrooms: {
      number_full: '3',
      number_half: '2'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: '2'
    },
    parking: {
      spaces: '2'
    },
    area: {
      square_feet: '1770'
    },
    amenities: ''
  },
  {
    title: 'Loading.. 5',
    _id: 'd5',
    address: {
      street_number: '160',
      street_name: 'Babcock St',
      town: 'Brookline',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '2'
    },
    bathrooms: {
      number_full: '1',
      number_half: '0'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '1'
    },
    area: {
      square_feet: '890'
    },
    amenities: ''
  },
  {
    title: 'Loading.. 6',
    _id: 'd6',
    address: {
      street_number: '37',
      street_name: 'Winchester St',
      town: 'Brookline',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '2'
    },
    bathrooms: {
      number_full: '2',
      number_half: '1'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '2'
    },
    area: {
      square_feet: '1760'
    },
    amenities: ''
  },
  {
    title: 'Loading.. 7',
    _id: 'd7',
    address: {
      street_number: '95',
      street_name: 'Heath St',
      town: 'Brookline',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '2'
    },
    bathrooms: {
      number_full: '2',
      number_half: '0'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '1'
    },
    area: {
      square_feet: '1246'
    },
    amenities: ''
  },
  {
    title: 'Loading.. 8',
    _id: 'd8',
    address: {
      street_number: '178',
      street_name: 'Brookline Ave',
      town: 'Brookline',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '2'
    },
    bathrooms: {
      number_full: '2',
      number_half: '1'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '1'
    },
    area: {
      square_feet: '1536'
    },
    amenities: ''
  },
  {
    title: 'Loading.. 9',
    _id: 'd9',
    address: {
      street_number: '15',
      street_name: 'School St',
      town: 'Brookline',
      state: 'Massachusetts',
    },
    bedrooms: {
      number: '5'
    },
    bathrooms: {
      number_full: '3',
      number_half: '1'
    },
    photos: {
      photo_count: '5',
      endings: null
    },
    market_time: {
      time: null
    },
    parking: {
      spaces: '3'
    },
    area: {
      square_feet: '2250'
    },
    amenities: ''
  },
];
