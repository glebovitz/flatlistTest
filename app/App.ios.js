import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  StatusBar,
  Dimensions,
} from 'react-native';

import _ from 'lodash';
import { NAVBAR_BACKGROUND_COLOR, STATUSBAR_HEIGHT } from './constants.js'

// pages
import PropertyListingsView from './components/PropertyListingsView';

// XXX: import { iconsMap, iconsLoaded } from './utils/AppIcons';

const styles = StyleSheet.create({
  navContainer: {
    flex: 1,
    height: Dimensions.get('window').height,
  },
  statusBar: {
    backgroundColor: NAVBAR_BACKGROUND_COLOR,
    height: STATUSBAR_HEIGHT,
  }
});

export default class App extends Component {

  constructor(props) {
    super(props);

  }

  render() {
    return (
      <View style={styles.navContainer}>
        <View style={{backgroundColor: NAVBAR_BACKGROUND_COLOR, height:20}}>
          <StatusBar
            backgroundColor={NAVBAR_BACKGROUND_COLOR}
            translucent={true}
            barStyle="dark-content"
          />
        </View>
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <PropertyListingsView
            ref={ref => this.listing = ref}
          />
        </View>
      </View>
    );
  }
}
