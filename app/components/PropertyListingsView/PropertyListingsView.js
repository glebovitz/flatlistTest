import React, { Component } from 'react';
import { Properties } from '../../../properties.js'
import {
  View,
  FlatList,
} from 'react-native';
import _ from 'lodash';

import ItemView from './ItemView';

import styles from './styles';
import { TOTAL_NAV_HEIGHT } from '../../constants';

export default class PropertyListingsView extends Component {
  constructor(props) {
    super(props);
  }

  //onPropertyChange = change => this.setState(change);

  render() {
    console.log("DEBUG: Properties ", Properties)
    return (
      <FlatList
        ref={ref => this._listView = ref}
        contentInset={{
          top: TOTAL_NAV_HEIGHT,
          left: 0,
          right: 0,
          bottom: 10,
        }}
        contentOffset={{
          x: 0,
          y: -TOTAL_NAV_HEIGHT
        }}
        directionalLockEnabled={true}
        horizontal={false}
        onScrollEventThrottle={1000}
        style={styles.propertyListView}
        data={Properties.results}
        extraData={this.props}
        keyExtractor={(item, index) => item._id}
        onEndReachedThreshold={0}

        // Listview Render Methods

        renderItem={item => {
            return (
              <View style={{ flex: 1, backgroundColor: '#000' }}>
                <ItemView
                  model={item.item}
                />
              </View>
         )}}
      />);
  }
}
