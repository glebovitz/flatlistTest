import { StyleSheet, Dimensions } from 'react-native';
import { TOTAL_NAV_HEIGHT, TILE_STYLE_VARS } from '../../constants';

export default styles = StyleSheet.create({
  centeredContainer: {
    flex: 1,
    alignSelf: 'stretch',
    height: Dimensions.get('window').height - TOTAL_NAV_HEIGHT,
    width: Dimensions.get('window').width,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    marginTop: TOTAL_NAV_HEIGHT,
  },

  propertyListView: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    flex: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'transparent',
  },

  photosScrollViewContainer: {
    flexGrow: 1,
    alignItems: 'flex-start',
    flexDirection: 'row',
    height: TILE_STYLE_VARS.tileHeight,
  },

  photosScrollView: {
    flexGrow: 1,
    height: TILE_STYLE_VARS.tileHeight,
    width: TILE_STYLE_VARS.tileWidth,
  },

  thumbContainer: {
    flexGrow: 1,
    height: TILE_STYLE_VARS.tileHeight,
    width: TILE_STYLE_VARS.tileWidth,
  },

  thumb: {
    flexGrow: 1,
    height: TILE_STYLE_VARS.tileHeight,
    width: TILE_STYLE_VARS.tileWidth,
    // backgroundColor: 'transparent'
  },

  propertyOuterContainer: {
    flex: 1,
    margin: 10,
    marginTop: 0,
    height: TILE_STYLE_VARS.tileHeight,
    width: TILE_STYLE_VARS.tileWidth,
    overflow: 'hidden',
    backgroundColor: '#333',
  },

  propertyCaption: {
    position: 'absolute',
    borderColor: 'transparent',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 0,
    paddingTop: 12,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 9,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },

  propertyCaptionText: {
    color: '#fff',
  },

  indicatorText: {
    color: '#fff',
    fontSize: 10,
    textAlign: 'center',
  },

  indicatorStatusNew: {
    color: '#EAFF76',
  },

  favoriteActivityIndicator: {
    height: 18,
    width: 18,
    marginTop: 2,
    opacity: 0.75,
  },

  propertyDetailContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 110,
    borderTopColor: '#222',
    borderTopWidth: 1,
    flexDirection: 'row',
  },

  propertyCallContainer: {
    height: 55,
    borderTopColor: '#222',
    borderTopWidth: 1,
    flexDirection: 'row',
  },

  propertyDetailContainerInfoSection: {
    flex: 3,
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },

  propertyDetailContainerInfoSectionRow: {
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },

  propertyDetailContainerInfoSectionText: {
    color: '#fff',
  },

  propertyDetailContainerButtonSection: {
    flex: 1,
    position: 'relative',
  },

  detailButton: {
    flex: 1,
    left: 0,
    right: 0,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#444',
  },

  agentCallButton: {
    flex: 1,
    left: 0,
    right: 0,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#494',
  },

  agentTextButton: {
    flex: 1,
    left: 0,
    right: 0,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#449',
  },

  moreInfoButton: {
    backgroundColor: '#555',
  }
});
