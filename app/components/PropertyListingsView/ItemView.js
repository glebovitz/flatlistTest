import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  View,
} from 'react-native';

import PhotosView from './PhotosView';
import styles from './styles';

export default class ItemView extends Component {
  static propTypes = {
    model: PropTypes.object,
  }

  constructor(props) {
    super(props);
  }

  // This method should set variables that will be picked up by
  // the mapsection component.

  render() {

    return (
      <View style={styles.propertyOuterContainer} shouldRasterizeIOS={true}>
        <View style={{ flex: 1, backgroundColor: '#000' }}>
          <PhotosView
            ref={ref => this._photosList = ref}
            data={this.props.model}
          />
        </View>
      </View>
    );
  }
}
