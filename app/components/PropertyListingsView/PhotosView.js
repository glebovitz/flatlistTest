import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { API_CONFIG } from '../../constants';

import {
  View,
  FlatList,
  Image,
} from 'react-native';

import styles from './styles';
import _ from 'lodash';

function  generatePhotoURLs(propData, limit = 50) {
  const id = propData._id;
  const { photo_count, uploaded_count, endings } = propData.photos;
  const photoCountLimit = uploaded_count || photo_count || 0;
  return _.range(photoCountLimit).map(index => {
    return `${API_CONFIG.propertyPhotosBase}${id}-${index}.${(endings && endings[`${index}`]) || 'jpg'}`
  });
}

export default class PhotosView extends Component {
  static propTypes = {
    data: PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.renderPhoto = this.renderPhoto.bind(this);
    this.photosURLs = generatePhotoURLs(props.data, 5);
    console.log("DEBUG: Constructor photoURLs", this.photosURLs)
  }

  renderPhoto(item) {
    const url = item.item;
    const rowID = item.index;
    return (
      <View style={styles.thumbContainer}>
        <Image
          style={styles.thumb}
          source={{ uri: url }}
        />
      </View>
    );
  }


  render() {
    return (
      <View shouldRasterizeIOS={true}>
        <FlatList
          ref={ref => this._photosListView = ref}
          horizontal={true}
          // onScrollEventThrottle={1000}
          style={styles.photosScrollView}
          contentContainerStyle={styles.photosScrollViewContainer}
          data={this.photosURLs}
          extraData={this.props}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderPhoto}
          initialNumToRender={1}
          directionalLockEnabled={true}
          pagingEnabled={true}
          shouldRasterizeIOS={true}
          // onEndReached={this.loadMorePhotos}
          emoveClippedSubviews={false}
        />
      </View>
    );
  }
}
