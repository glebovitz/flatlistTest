#! /opt/local/bin/perl
use JSON::Parse 'parse_json';
use List::Util qw(min max);

$THIS_HOST = 192.168.1.121;
$THIS_PORT = 8013;

$API_CONFIG = {
  dev_mode => true,
  domain => "http://$THIS_HOST:$THIS_PORT',
  propertyPhotosBase => 'https://f000.backblazeb2.com/file/properties/',
  propertiesBase => '/api/properties',
  locationBase => '/api/location',
};

$/=undef;
$_ = <STDIN>;
s/export\s+const\s+Properties\s*=\s*//;
$json=parse_json($_);
$properties=%$json{"results"};
foreach $prop (@$properties) {
	$id = %$prop{"_id"};
	$photoCount = %$prop{"photos"}->{"photo_count"};
	$uploadCount = %$prop{"photos"}->{"uploaded_count"};
	$count = min($photoCount, $uploadCount);
	map {
		$url = $API_CONFIG->{"propertyPhotosBase"}."$id-$_.jpg";
		print $url, "\n";
		# system ("/opt/local/bin/wget",$url) == 0 or print "system failed: $?"
		# 	if ! -e "$id-$_.jpg";
	} (0..$count-1);

};


