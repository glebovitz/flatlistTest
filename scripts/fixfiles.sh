#! /bin/bash
pattern='\(void \(\^\)\(\)\)'
replace='(void (^)(void))'

files=( $(grep -E -r -l -e "${pattern}" ./node_modules/react-native) )
if [ "${#files}" -gt '0' ]; then
  for file in ${files[*]}; do
    echo ${file}
    tmpFilePattern=${file}.XXXXX
    tmpFile=$(mktemp ${tmpFilePattern}) || exit 1
    sed -E -e "s/${pattern}/${replace}/" $file > ${tmpFile}
    mv ${tmpFile} ${file}
  done
fi
